# overall purpose

The overall purpose is to create a todolist which can allows the user to easily organize tasks by the date they are due.

# functional examples

The program will be run from the command line and will be given the schedule file:
```
$ ruby src/todo.rb data/schedule.txt
```

The schedule will be presented being sorted in chronological order. If tasks are overdue, they will be at the top. The user can press 'C' to complete a task, 'A' to add a task, 'E' to edit a task, 'R' to remove a task, 'H' to display help, and 'X' to exit. The todolist will look like this:
```
========== May 10 2021 ==========
[ ] Work on Todolist CLI    #projects
[X] Laundry                 #other
>>
```

The tasks will be displayed if they were not already due or if they haven't been completed.

The tasks take the following form:
```
[!] [#tag] [@date] Task
```
* `!` indicates that the task is complete
* `#tag` is the user given tag for groups of tasks, (i.e. #work, #school)
* `@date` is the date the task is due.
* `Task` is the given task.

This is how the tasks are stored in the tasks file. It is also how the user inputs a task. By default, the task is incomplete, the tag is empty, and the date is today. The tasks will be updated in the provided schedule file as they are added, modified, and removed.

Dates will be stored in the file using the `Date.to_s` representation: `"2021-05-09"`

Input dates will be read (primarily) using `Date.parse()`. This will work for the following formats (among others):
* yyyy-mm-dd
* mm-dd
* day of week (i.e. "mon", "friday", etc.)
* any variation of "tomorrow"

# data flow

Uses the following:
* `Date` for storing the dates of tasks
* `term/ansicolor` to simplify coloring and bolding output.
* `terminfo` to get info, such as width/height about terminal
* `io/console` to use `STDIN.getch` to get character from user

Classes:
* todo.rb
  * main entry point of program
  * gives tasks to display and modifies the list of tasks based on user input
* taskMaker.rb
  * reads tasks from file and writes to file
* task.rb
  * data container for tasks
  * able to parse the task
* display.rb
  * `display_tasks(tasks, menu, prompt)` displays the (given) tasks and dates, the menu returns user inputs.
  * `select_task(tasks, menu)` allows user to select a task uses `format_tasks` to get the tasks and provides a menu to `display_tasks` for selection.
  * `format_tasks(tasks, number_tasks)` is used to return a formatted list of the tasks -- used by `display_tasks` and `select_task`. this is given a list of tasks & dates. if `number_tasks` is true, the tasks are giving the index (i.e. in context of selecting a task).
  * `show(tasks, menu)` shows the given list of tasks and returns menu input
* menu.rb
  * can either get character, or string input

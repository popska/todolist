require_relative 'task'

class TaskMaker
  def self.read_tasks(file_name)
    tasks = []
    open(file_name) do |file|
      while line = file.gets
        tasks << Task.new(line)
      end
    end
    tasks
  end

  def self.write_tasks(file_name, tasks)
    open(file_name, 'w') do |file|
      tasks.each do |task|
        file.write(task.to_file, "\n")
      end
    end
  end
end

require 'date'

class Task < include Comparable

  attr_accessor :due_date
  attr_reader :task_body, :tag, :completed

  def initialize(task)
    @task = task

    # default values
    @task_body = ""
    @tag = ""
    @due_date = Date.today
    @completed = false

    parse_task(@task)
  end

  def toggle_completed
    @completed = !@completed
  end

  def <=>(other)
    -(@due_date <=> other.due_date)
  end

  def to_file
    task_f = @completed ? "! " : ""
    task_f += "@" + @due_date.to_s
    task_f += " " + @tag
    task_f += " " + @task_body
  end

  def parse_task(input)
    if is_complete = input[/^!/]
      input.sub!('!', '')
      @completed = true
    end

    if tag = input[/#\w+/]
      input.sub!(tag, '')
      @tag = tag
    end

    if date = input[/@(\w|-)+/]
      input.sub!(date, '')
      if "@tomorrow".include?(date)
        @due_date = Date.today + 1
      else
        @due_date = Date.parse(date) rescue Date.today
        if @due_date < Date.today && date[/\d+/].nil?  # inputs like '@sun', '@friday' will go to the next sunday/friday 
          @due_date += 7
        end
      end
    end
    input.strip!
    @task_body = input
  end
end

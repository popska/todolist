require 'terminfo'
require 'term/ansicolor'

include Term::ANSIColor

class Display

  # NOTE: tasks also contains the dates
  def format_tasks(tasks, number_tasks)
    @rows, @cols = TermInfo.screen_size
    
    body_width = @cols / 2
    tag_width = @cols / 6
    left_padding = @cols / 6
    number = tasks.select{|task| task.is_a?(Task)}.length - 1  # count in descending order
    formatted = []

    (tasks).each do |task|
      if task.is_a?(Task)
        box = task.completed ? "[X]" : "[ ]"
        task = sprintf("%#{left_padding}s %-#{body_width}s %-#{tag_width}s", box, task.task_body, task.tag)
        if number_tasks
          task.sub!(" " * (left_padding - 5), sprintf("%#{left_padding - 5}d", number))
          number -= 1
        end
        task.center(@cols)
        formatted << task
      elsif task.is_a?(Date)
        formatted << "\n\n"
        date = ("   -==========-   " + task.to_s + "   -==========-   ").center(@cols)
        date = task < Date.today ? blink(magenta(date)) : date = yellow(date)
        formatted << date
      end
    end
    formatted
  end

  def show(tasks, menu)
    # insert blank space above display
    (@rows - tasks.length).times do
      puts
    end
    
    (tasks).each do |task|
      puts task
    end
    puts
    menu.get_input
  end

  def display_tasks(tasks, menu, prompt)
    tasks = format_tasks(tasks, false)
    tasks += prompt
    show(tasks, menu)
  end

  def select_task(tasks, menu)
    tasks = format_tasks(tasks, true)
    show(tasks, menu)
  end
end

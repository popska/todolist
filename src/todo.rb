require_relative 'task_maker'
require_relative 'display'
require_relative 'menu'


class Todo
  def self.run
    @tasks = TaskMaker.read_tasks(ARGV[0])
    @tasks.sort!.select! { |task| task.due_date >= Date.today || !task.completed}  # don't use tasks which are completed and were due previously
    @d = Display.new
    @tasks_and_dates = self.get_dates
    self.display_tasks
  end

  def self.display_tasks

    tasks_menu = Menu.new(:short)
    prompt = []

    while selection = @d.display_tasks(@tasks_and_dates, tasks_menu, prompt)
      prompt = []
      if selection == 'h'
        prompt = self.help_message
      elsif selection == 'c'
        self.complete_task
      elsif selection == 'a'
        self.add_task
      elsif selection == 'e'
        self.edit_task
      elsif selection == 'r'
        self.remove_task
      elsif selection == 's'
        self.reschedule_overdue
      elsif selection == 'x'
        puts
        break
      else
        prompt = ["invalid input '#{selection}', press 'h' for help"]
      end
      TaskMaker.write_tasks(ARGV[0], @tasks)
    end
  end

  def self.help_message
    help = [
      "h -- display this Help menu",
      "c -- mark task as Complete",
      "a -- Add new task",
      "e -- Edit a task",
      "r -- Remove a task",
      "x -- eXit program",
      "s -- reSchedule overdue tasks"
    ]
  end

  def self.get_dates
    old_date = nil
    tasks = @tasks.clone
    (tasks).each do |task|
      if task.due_date != old_date
        old_date = task.due_date
        tasks.insert(tasks.find_index(task), task.due_date)
      end
    end
    tasks
  end

  def self.add_task
    add_text = ["Enter new task: "]
    add_menu = Menu.new(:long)
    new_task = @d.show(add_text, add_menu)
    @tasks << Task.new(new_task)
    @tasks.sort!
    @tasks_and_dates = self.get_dates
  end

  def self.select_task
    select_menu = Menu.new(:long, "Enter task #: ")
    while true
      task_index = Integer(@d.select_task(@tasks_and_dates, select_menu)) rescue -1
      if 0 <= task_index && task_index < @tasks.length
        return @tasks.length - task_index - 1  # fix for descending numbering
      else
        select_menu.prompt = "Enter task # [0 - #{@tasks.length}]: "
      end
    end
  end

  def self.edit_task
    # TODO: prepopulate task field

    task_index = self.select_task

    edit_text = ["Editing: #{@tasks[task_index].to_file}", "", "Updated task: "]
    edit_menu = Menu.new(:long)
    new_task = @d.show(edit_text, edit_menu)

    # Delete and add task
    @tasks.delete_at(task_index)
    @tasks << Task.new(new_task)
    @tasks.sort!
    @tasks_and_dates = self.get_dates
  end

  def self.remove_task
    task_index = self.select_task
    @tasks.delete_at(task_index)
    @tasks_and_dates = self.get_dates
  end

  def self.complete_task
    task_index = self.select_task
    @tasks[task_index].toggle_completed
    @tasks_and_dates = self.get_dates
  end

  def self.reschedule_overdue
    date_prompt = ["Enter new date for overdue tasks"]
    date_menu = Menu.new(:long)
    date = Task.new(@d.show(date_prompt, date_menu)).due_date

    @tasks.each do |task|
      if task.due_date < Date.today
        task.due_date = date
      end
    end

    @tasks.sort!
    @tasks_and_dates = self.get_dates
  end

  if ARGV.length < 1
    puts "include todo file argument"
    exit
  else
    self.run
  end
end

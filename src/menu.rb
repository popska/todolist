require 'io/console'

class Menu
  attr_accessor :prompt

  def initialize(type, prompt=">>")
    @type = type
    @prompt = prompt
  end

  def get_input
    print(@prompt)
    if @type == :long
      input = STDIN.gets
    elsif @type == :short
      input = STDIN.getch
      if input == "\u0003"  # handle ctrl-c
        puts "\nsignal interupt"
        exit
      end
    end
    input
  end
end
